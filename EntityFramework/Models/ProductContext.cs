﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EntityFramework.Models
{
    public class ProductContext : DbContext
    {

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public ProductContext():base("EfDbContext") // ne mora ako su naziv klase i conn stringa isti
        {

        }



    }
}