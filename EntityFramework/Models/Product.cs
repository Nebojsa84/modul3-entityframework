﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EntityFramework.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public decimal? Price { get; set; }

        [ForeignKey("CategoryId")]
        public ProductCategory Category { get; set; }
        
        public int CategoryId { get; set; }
    }
}